/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <zephyr/sys/printk.h>
#include <zephyr/sys/util.h>

#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

#define BT_ADV_SCAN_UNIT_MS(_ms) ((_ms) * 8 / 5)

struct my_beacon_payload {
	uint16_t company_id;
    uint8_t tag[8];           // Tag
    uint32_t value;           // Value
} payload = {
	.company_id = BT_COMP_ID_LF,
	.tag = "michelin",
	.value = 0,
};

static struct bt_le_ext_adv *adv;

const struct bt_data ad = {
	.type = BT_DATA_MANUFACTURER_DATA,
	.data_len = sizeof(struct my_beacon_payload),
	.data = (const uint8_t*)&payload,
};

void start_advertising(void);

void adv_sent_cb(struct bt_le_ext_adv *adv, struct bt_le_ext_adv_sent_info *info)
{
	int err;

    printk("Advertisement sent successfully!\n");

    // Stop the current advertising
    err = bt_le_ext_adv_stop(adv);
    if (err) {
        printk("Failed to stop advertising (err %d)\n", err);
        return;
    }

	// wait the amount of time before restarting to advertise
	k_sleep(K_SECONDS(1));
    // Restart the advertising
    start_advertising();
}

static struct bt_le_ext_adv_cb adv_cb = {
    .sent = adv_sent_cb,
    .connected = NULL,
	.scanned = NULL
};

void create_adv_set(void)
{
	struct bt_le_adv_param adv_param = {
		.id = BT_ID_DEFAULT,
		.interval_min = BT_GAP_ADV_FAST_INT_MIN_1,
		.interval_max = BT_GAP_ADV_FAST_INT_MAX_1,
		.options = BT_LE_ADV_OPT_USE_IDENTITY,
	};

    int err = bt_le_ext_adv_create(&adv_param, &adv_cb, &adv);
    if (err) {
        printk("Failed to create advertising set (err %d)\n", err);
        return;
    }
}

void start_advertising(void)
{
    int err;

    err = bt_le_ext_adv_set_data(adv, &ad, 1, NULL, 0);
    if (err) {
        printk("Failed to set advertising data (err %d)\n", err);
        return;
    }

	struct bt_le_ext_adv_start_param start_param = {
		.num_events = 1
	};

    err = bt_le_ext_adv_start(adv, &start_param);
    if (err) {
        printk("Failed to start advertising (err %d)\n", err);
        return;
    }

    printk("Advertising started\n");
}

int main(void)
{
	int err;

	printk("Starting Beacon Demo\n");

	/* Initialize the Bluetooth Subsystem */
  	err = bt_enable(NULL);
    if (err) {
        printk("Bluetooth init failed (err %d)\n", err);
        return -1;
    }

	printk("Bluetooth initialized\n");

	create_adv_set();
    start_advertising();

}
