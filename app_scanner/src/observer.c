/*
 * Copyright (c) 2022 Nordic Semiconductor ASA
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/sys/printk.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/hci.h>

#define NAME_LEN 30

struct my_beacon_payload {
	uint8_t tag[8];
	uint32_t value;
};

struct bt_pkt_info {
	char le_addr[BT_ADDR_LE_STR_LEN];
	char name[NAME_LEN];
	struct my_beacon_payload data;
	uint16_t data_len;
	int8_t rssi;
};

static bool data_cb(struct bt_data *data, void *user_data)
{
	struct bt_pkt_info *bt_pkt_info = (struct bt_pkt_info *)user_data;

	switch (data->type) {
    case BT_DATA_MANUFACTURER_DATA:
		(void)memcpy(&bt_pkt_info->data, data->data, sizeof(struct my_beacon_payload));

		if(strcmp (bt_pkt_info->data.tag, "abcdtest") == 0){
			printk("[DEVICE]: %s, AD evt type %u, RSSI %i "
				" AD data len: %u, AD tag : %s : val : %d\n",
				bt_pkt_info->le_addr, data->type, 
				bt_pkt_info->rssi, bt_pkt_info->data_len,
				bt_pkt_info->data.tag, bt_pkt_info->data.value);
		}

		return false;
    
	default:
		return true;
	}
}

static void scan_cb(const bt_addr_le_t *addr, int8_t rssi, uint8_t type,
			 struct net_buf_simple *ad)
{
	struct bt_pkt_info bt_pkt_info;

	(void)memset(bt_pkt_info.name, 0, sizeof(bt_pkt_info.name));

	bt_pkt_info.data_len = ad->len;
	bt_pkt_info.rssi = rssi;

	bt_addr_le_to_str(addr, bt_pkt_info.le_addr, sizeof(bt_pkt_info.le_addr));

	bt_data_parse(ad, data_cb, &bt_pkt_info);
}

int observer_start(void)
{
	struct bt_le_scan_param scan_param = {
		.type       = BT_LE_SCAN_TYPE_PASSIVE,
		.options    = BT_LE_SCAN_OPT_FILTER_DUPLICATE,
		.interval   = BT_GAP_SCAN_FAST_INTERVAL,
		.window     = BT_GAP_SCAN_FAST_WINDOW,
	};
	int err;

	err = bt_le_scan_start(&scan_param, scan_cb);
	if (err) {
		printk("Start scanning failed (err %d)\n", err);
		return err;
	}
	printk("Started scanning...\n");

}